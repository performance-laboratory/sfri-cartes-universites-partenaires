document.addEventListener("DOMContentLoaded",  function() {
  init();
}, false);

function init() {
	
    var url = "api/universites";
	ajaxCall(url);

    // event pour les filtres location et type de rÃ©sidences
    var filtres = document.querySelectorAll("#filters input");
    filtres.forEach(el => el.addEventListener("click", checkboxInteractions));

    // event pr afficher/cacher les marqeurs d'univ
    var input_marker = document.querySelector("#toggle_markers");
    input_marker.addEventListener("click", toggleMarkers);
}

function toggleMarkers() {
    this.nextElementSibling.classList.toggle("active");
    var marker_pane = document.querySelector(".leaflet-pane.leaflet-marker-pane");
    marker_pane.classList.toggle("visible");
}

function checkboxInteractions() {
    this.nextElementSibling.classList.toggle("active");

    var selectorSelection = "path.leaflet-interactive";
    var selectorSelection_popup = ".leaflet-popup-content > div > ul > li";
    var selectorContainerInput = "#filters";
    var selectorContainer = "#mapid";
    var data_attr = "data-cat";

    var resetCheckbox = false;
    var operator = "and";
    var showConsole = false;

    var els_popup = document.querySelectorAll(selectorSelection_popup);
    var deja_selected = document.querySelectorAll(selectorContainerInput + ' input:checked');

    if (resetCheckbox) {
        for (var i = 0; i < deja_selected.length; i++) {
            if (this != deja_selected[i]) deja_selected[i].checked = false;
        }
        var deja_selected = document.querySelectorAll(selectorContainerInput + ' input:checked');
    }
    var tableau_choix = [];
    for (var i = 0; i < deja_selected.length; i++) {
        tableau_choix.push(deja_selected[i].value);
    }

    if (showConsole) console.log(tableau_choix);

    // reset 
    var el_deja_selected = document.querySelectorAll(selectorSelection+'.selected');
    for (var i = 0; i < el_deja_selected.length; i++) {
        el_deja_selected[i].classList.remove("selected");
    }
    var el_popup_deja_selected = document.querySelectorAll(selectorSelection_popup+'.selected');
    for (var i = 0; i < el_popup_deja_selected.length; i++) el_popup_deja_selected[i].classList.remove("selected");

    // collection d'univs correspondants aux choix
    // pr ensuite faire une collection des pays concernés
    var univ_filtered = [];

    // opérateur de conditions
    var ou = (operator === "or") ? true : false;
    for (var j = 0; j < dataset.length; j++) {
        var univ_cats = dataset[j].categories;

        if (ou) {
            var results = tableau_choix.some(function(element){
                return univ_cats.indexOf(element) != -1;
            });
        } else {
            var results = tableau_choix.every(function(element){
                return univ_cats.indexOf(element) != -1;
            });
        }

        if (results) {
            univ_filtered.push(dataset[j]);
        }
    }
    if (showConsole) console.log(univ_filtered);
    updateCount(tableau_choix, univ_filtered);

    var countries =  [...new Set(univ_filtered.map(univ => univ.country))];
        var els = countries.map(country => document.querySelector(selectorSelection+'[data-country="'+country+'"]'));
   
    // collection des résultats
    var set_resultat = new Set(els);

    // popup
    for (var j = 0; j < els_popup.length; j++) {
        var datas = els_popup[j].getAttribute(data_attr);

        if (ou) {
            var results = tableau_choix.some(function(element){
                return datas.indexOf(element) != -1;
            });
        } else {
            var results = tableau_choix.every(function(element){
                return datas.indexOf(element) != -1;
            });
        }

        if (results) {
            set_resultat.add(els_popup[j]);
        }
    }

    // sélection des résultats front-end
    for (let item of set_resultat) item.classList.add("selected");

    var container = document.querySelector(selectorContainer);
    if (tableau_choix.length) {
        if (showConsole) console.log(set_resultat);
      
        container.classList.add("filtered");

        // si aucun résultats
        if (!set_resultat.size) {
            if (showConsole) console.log("aucun résultats");
        } else {
            if (showConsole) console.log(set_resultat.size + " résultats");
        }
    } else {
        if (showConsole) console.log("remove filtered");
        container.classList.remove("filtered");
    }

}

function updateCount(choices, univs) {
    var filtres = document.querySelectorAll("#filters input");
    for (var i = 0; i < filtres.length; i++) {
        var value = filtres[i].value;
        // console.log(filtres[i]);
        var univs_filtered = univs.filter(univ => univ.categories.indexOf(value) != -1);
        var count = univs_filtered.length;
        var span_count = filtres[i].nextElementSibling.querySelector("span");
        span_count.innerHTML = '('+count+')';
    }
}


function updateMapZoom() {
	var width = document.documentElement.clientWidth;
	// console.log(width);
	if (width < 576) {
        mymap.setZoom(1.4);
    } else {
    	mymap.setZoom(2);
    }
}


window.addEventListener('resize', function(event){
    // get the width of the screen after the resize event
	updateMapZoom();
});


function ajaxCall(url) {
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", url, true);
    xhttp.setRequestHeader("X-Requested-With", "XMLHttpRequest");

    xhttp.onreadystatechange = function() {
        xhttp.getAllResponseHeaders();
        if (this.readyState == 4 && this.status == 200) {
            // console.log(this.responseText);
            initmap(this.responseText);
        }
    };
    
    xhttp.send();
}


async function request(url, params, method = 'GET') {
    var options = {
        method,
        mode: "cors"
    };
    var response = await fetch(url, options);
    if (response.status != 200) {
        return "unexpected status";
    }
    var json = await response.json();
    return json;
}
async function initmap(data) {
    
    mymap = L.map('mapid').setView([5, 0], 2);
    updateMapZoom();
    // 'https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png'
    // '&copy; Openstreetmap France | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    // 'http://{s}.tile.osm.org/{z}/{x}/{y}.png'
    // '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors'
    /*
    var tileLayer = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoidm1haWxsYXJkIiwiYSI6ImNqc2VmNXdmdzE1cjU0NGw5eGZjMHdicnAifQ.IoSI4o23AwMGNj39tqRjAw', {
        maxZoom: 18,
        minZoom: 1,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox.light',
        noWrap: true,
        zoomDelta: 0.5,
        closePopupOnClick: true
    });
    */
    
    var tileLayer = L.tileLayer('', {
        maxZoom: 8,
        minZoom: 1,
        noWrap: true,
        zoomDelta: 0.5,
        closePopupOnClick: true
    });
    tileLayer.addTo(mymap);
    

    dataset = JSON.parse(data).universites;
    var myStyle = {
        "color": "#4705b4",
        "fillColor": "#ddd",
        "fillOpacity": 1,
        "weight": 1,
        "opacity": 1
    };

    // test pr add polygon countries 
    var url = "site/assets/files/data/countries.geo.json";
    var data = await request(url);
    // console.log(data);

    var geoJsonLayer = L.geoJson(data, {
        style: myStyle
    }).addTo(mymap);

    geoJsonLayer.eachLayer(function (layer) {
        var feature = layer.feature;
        var univ_filtered = dataset.filter(univ => univ.country === feature.properties.name);
        if (univ_filtered.length) {
            layer.setStyle({ fillColor :'#4705b4', color: "#ddd" });
            var map_ids = univ_filtered.map(univ => univ.id);

            var div = document.createElement("DIV");
            var h4 = document.createElement("H4");
            h4.innerHTML = feature.properties.name;

            var ul = document.createElement("UL");
            for (var i = 0; i < univ_filtered.length; i++) {
                var li = document.createElement("LI");
                var span = document.createElement("SPAN");
                span.innerHTML = univ_filtered[i].title;
                li.appendChild(span);
                li.setAttribute("data-cat", univ_filtered[i].categories);
                li.addEventListener("click", showCategories);
                ul.appendChild(li);
            }
            div.appendChild(h4);
            div.appendChild(ul);
            layer.bindPopup(div);
            var dataset_cats = univ_filtered.map(univ => univ.categories.split(" "));
            var cats = [...new Set(dataset_cats.flat())];
            // layer._path.setAttribute("data-cat", cats.join(" "));
            layer._path.setAttribute("data-country", feature.properties.name);
        } else {
            layer._path.setAttribute("data-cat", "");
        }
        // console.log(univ_filtered);
    });
    
    // fermeture .subpopup à la fermeture du popup
    mymap.on('popupclose', function(e) {
        var subpopup = document.querySelector(".subpopup");
        if (subpopup) subpopup.parentNode.removeChild(subpopup);
    });
    
    mymap.on('popupopen', function(e) {
        var selectorContainerInput = "#filters";
        var selectorSelection_popup = ".leaflet-popup li";
        var data_attr = "data-cat";
        var operator = "and";

        var deja_selected = document.querySelectorAll(selectorContainerInput + ' input:checked');
        var tableau_choix = [];
        for (var i = 0; i < deja_selected.length; i++) {
            tableau_choix.push(deja_selected[i].value);
        }
        // reset
        var el_popup_deja_selected = document.querySelectorAll(selectorSelection_popup+'.selected');
        for (var i = 0; i < el_popup_deja_selected.length; i++) el_popup_deja_selected[i].classList.remove("selected");
        // collection des résultats
        var set_resultat = new Set();
        // opérateur de conditions
        var ou = (operator === "or") ? true : false;
        // popup
        var els_popup = e.popup._container.querySelectorAll(selectorSelection_popup);
        for (var j = 0; j < els_popup.length; j++) {
            var datas = els_popup[j].getAttribute(data_attr);
            if (ou) {
                var results = tableau_choix.some(function(element){ return datas.indexOf(element) != -1; });
            } else {
                var results = tableau_choix.every(function(element){ return datas.indexOf(element) != -1; });
            }
            if (results) set_resultat.add(els_popup[j]);
        }

        // sélection des résultats front-end
        for (let item of set_resultat) item.classList.add("selected");
    });

    

    
    // map avec cluster, sans control
    var greenIcon = L.icon({
        iconUrl: 'site/assets/files/img/marker-icon.png',
        
        iconSize: [20,27],
        iconAnchor: [10,23.5],
        popupAnchor:  [0, -20],
        slug: "salt"
        // iconSize:     [40, 47],
        // iconAnchor:   [40, 47]
    });

    markers = L.markerClusterGroup();
    for (var i = 0; i < dataset.length; i++) {
        if (dataset[i].coordinates != undefined && dataset[i].coordinates.length) {
            var lat = dataset[i].coordinates[0];
            var lon = dataset[i].coordinates[1];
            var marker = L.marker(
                    new L.LatLng(lat, lon),
                    { icon: greenIcon }
                )
                .bindPopup(dataset[i].title);
            markers.addLayer(marker);
        }
    }
    mymap.addLayer(markers);
        
    /*
    // map sans cluster, sans control avec une possible gestion de catégories
    // avec changement de couleur de curseurs possibles si cat uniq
    var markers = {};
    for (var i=0 ; i < dataset.length ; i++) {
        if (dataset[i].coordinates != undefined) {

            var uniq_id = dataset[i].id;

            var lat = dataset[i].coordinates[0];
            var lon = dataset[i].coordinates[1];

            if (dataset[i].categories == 1053) {
                var icon_url = "red-marker-icon.png";
            } else {
                var icon_url = "marker-icon.png";
            }
            var Icon = L.icon({
                iconUrl: 'site/assets/files/img/'+icon_url,
                iconSize: [20,27],
                iconAnchor: [10,23.5],
                popupAnchor:  [0, -20],
                slug: "salt"
                // iconSize:     [40, 47],
                // iconAnchor:   [40, 47]
            });

            markers[dataset[i].datacats] = L.marker([lat, lon], {icon: Icon}).addTo(mymap).bindPopup(dataset[i].title);
            markers[dataset[i].datacats]._icon.setAttribute("data-cat", dataset[i].categories);
            
        }
    }
    */
    
    /*
    var markers = {};
    for (var i=0 ; i<dataset.length ; i++) {
        if (dataset[i].coordinates.length) {

            var uniq_id = dataset[i].id;

            var lat = dataset[i].coordinates[0];
            var lon = dataset[i].coordinates[1];

            var popup_link = document.createElement("A");
            popup_link.innerHTML = dataset[i].title;
            popup_link.href = "";
            popup_link.setAttribute("data-id", dataset[i].id);
            popup_link.addEventListener("click", scrollToRes);

            markers[uniq_id] = L.marker([lat, lon], {icon: greenIcon}).addTo(mymap).bindPopup(popup_link);
            markers[uniq_id]._icon.setAttribute("data-cat", dataset[i].datacats);            
        }
    }
    */
    
    /*
    // map sans cluster, avec control
    // avec doublon quand un item appartient à pls catégories
    var greenIcon = L.icon({
        iconUrl: 'site/assets/files/img/marker-icon.png',
        iconSize: [20,27],
        iconAnchor: [10,23.5],
        popupAnchor:  [0, -20],
    });

    var cats = ["1058", "1059", "1060"];
    var control = L.control.layers(null, null, { collapsed: false });
    for (var i = 0; i < cats.length; i++) {
        var cat = cats[i];
        var result = dataset.filter(item => item.categories.includes(cat));
        var markers = [];
        var groupLayer = L.layerGroup();
        for (var j = 0; j < result.length; j++) {
            if (result[j].coordinates != undefined) {
                var lat = result[j].coordinates[0];
                var lon = result[j].coordinates[1];
                var marker = L.marker([lat, lon], {icon: greenIcon});
                markers.push(marker);
                groupLayer.addLayer(marker);
            }
        }
        groupLayer.addTo(mymap);
        control.addOverlay(groupLayer, cat);
        control.addTo(mymap);
    }
    */
    
    /*
    // map avec cluster et avec control
    // avec doublon quand un markers qappartient à pls catégories
    var greenIcon = L.icon({
        iconUrl: 'site/assets/files/img/marker-icon.png',
        iconSize: [20,27],
        iconAnchor: [10,23.5],
        popupAnchor:  [0, -20],
    });

    var dataset_cats = dataset.map(univ => univ.categories.split(" "));
    var cats = [...new Set(dataset_cats.flat())];
    console.log(cats);
    var control = L.control.layers(null, null, { collapsed: false });
    var mcg = L.markerClusterGroup();

    for (var i = 0; i < cats.length -1; i++) {
        var cat = cats[i];
        var result = dataset.filter(item => item.categories.includes(cat));
        var markers = [];
        var groupLayer = L.featureGroup.subGroup(mcg);
        for (var j = 0; j < result.length; j++) {
            if (result[j].coordinates != undefined) {
                var lat = result[j].coordinates[0];
                var lon = result[j].coordinates[1];
                var marker = L.marker([lat, lon], {icon: greenIcon});
                marker.bindPopup(result[j].title);
                markers.push(marker);
                groupLayer.addLayer(marker);
            }
        }
        mcg.addTo(mymap);
        control.addOverlay(groupLayer, cat);
        control.addTo(mymap);
    }
    */
    
    // exception si pas de catégories
    // dataset
    
    
}


/*
    Event appelé si une popup est ouverte
    à chaque clic sur une université d'un pays.
    Ajoute un .subpopup en-dessous de l'univ
*/
function showCategories() {
    var sub = document.querySelector(".subpopup");
    var popup = document.querySelector(".leaflet-popup-content");
    if (sub) sub.parentNode.removeChild(sub);

    var cats = this.getAttribute("data-cat");
    var div = document.createElement("DIV");
    div.classList.add("subpopup");

    var div_content = document.createElement("DIV");
    
    var h4 = document.createElement("H4");
    h4.innerHTML = "Catégories";
    div_content.appendChild(h4);

    var cats_ul = alignCatsWithTitle(cats);
    if (!cats_ul.childNodes.length) cats_ul.innerHTML = "Pas de catégorie pour cette université"
    div_content.appendChild(cats_ul);
    div_content.classList.add("content");

    var a = document.createElement("A");
    a.innerHTML = "×";
    a.classList.add("subpopup-close-button");
    a.addEventListener("click", closeSubPopup);
    div_content.appendChild(a);

    div.appendChild(div_content);

    var tip = document.createElement("DIV");
    tip.classList.add("leaflet-popup-tip");
    div.appendChild(tip);
    
    this.appendChild(div);
}

/*
    Event appelé au clic sur une croix d'un subpopup.
    Supprime le .subpopup
*/
function closeSubPopup(e) {
    e.stopPropagation();
    this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
}

function alignCatsWithTitle(str) {
    var filters = document.querySelectorAll("#filters label");
    var arr = str.split(" ");
    
    
    var title_arr = [];
    for (var i = 0; i < arr.length; i++) {
        var input = document.querySelector("#filters label input[value='"+arr[i]+"']");
        if (input) title_arr.push(input.parentNode.querySelector(".cat_title").firstChild.textContent);
    }
    var ul = document.createElement("UL");
    for (var i = 0; i < title_arr.length; i++) {
        var li = document.createElement("LI");
        li.innerHTML = title_arr[i];
        ul.appendChild(li);
    }
    return ul;
    

    /*
    ⬓ ALC - UFR LLASIC
    ◧ Création artistique - UFR LLASIC
    ⬒ Histoire - UFR arts et sciences humaines
    ◨ Histoire de l’art - UFR arts et sciences humaines
    ◩ LLCER - UFR langues étrangères

    ◒ Accord cadre bilatéral
    ◓ Accord cadre multilatéral
    ◑ Accord double diplôme
    ◐ Accord erasmus+
    ⬖ Accord recherche
    */
    /*
    var data = {
        1089: "⬓",
        1088: "◧",
        1086: "⬒",
        1087: "◨",
        1090: "◩",
        1072: "◒",
        1074: "◓",
        1078: "◑",
        1080: "◐",
        1084: "⬖"
    };
    var title_arr = [];
    for (var i = 0; i < arr.length; i++) {
        var input = document.querySelector("#filters label input[value='"+arr[i]+"']");
        if (input) {
           title_arr.push(data[arr[i]]);
        }
    }
    var span = document.createElement("SPAN");
    span.innerHTML = title_arr.join(" ");
    return span;
    */
}