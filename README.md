# Carte d'universités partenaires

Site Profile pour ProcessWire 3.0.

Développé dans le cadre du [CDP Performance Laboratory](https://performance.univ-grenoble-alpes.fr/).



## Liens

- [site](http://performance-lab.huma-num.fr/carte_univ/)
- [admin](http://performance-lab.huma-num.fr/carte_univ/processwire/)
- [api](http://performance-lab.huma-num.fr/carte_univ/api/universites)



## Installation

- télécharger [ProcessWire](https://processwire.com/download/core/) et l'installer selon "blank profile" 
- cloner le git dans le dossier `site/templates` de cette installation ProcessWire
- importer la db `_sfr_carte_univ_pw.sql` dans la table dédiée à l'installation
- installer les modules listés ci-dessous (fichiers dans le dossier `site/modules`) : soit en les téléchargeant à la main soit en les installant par l'admin de ProcessWire
- dans l'admin ProcessWire, dans la config du module RestApi (modules > site > RestApi), renseigner l'endpoint API souhaité ("carte_univ/api" si dans le sous-dossier "carte_univ" + si différent de /api à la fin, modifier dans `site/templates/scripts/main.js`)
- placer le dossier `site/templates/export/api/` dans `site/`
- placer le dossier `site/templates/export/data/` dans `site/assets/files`
- placer le dossier `site/templates/export/img/` dans `site/assets/files`
- dans `site/templates/scripts/` faire `npm install`
- dans `site.config.php` ajouter : `$config->prependTemplateFile = '_init.php';`



## Requirements

[Voir ici.](https://processwire.com/about/requirements/)

- droit d'écriture récursifs sur les dossiers `site/assets` et `site/modules`
- A Unix or Windows-based web server running Apache or compatible
- PHP version 5.4 or newer with PDO database support (PHP 7+ preferable)
- MySQL or MariaDB, 5.0.15 or greater (5.5+ preferable)
- Apache must have mod_rewrite enabled
- Apache must support .htaccess files
- PHP's bundled GD 2 library or ImageMagick library



## Modules

- [Leaflet Map Marker 2.8.1](https://modules.processwire.com/modules/fieldtype-leaflet-map-marker/) Field that stores an address with latitude and longitude coordinates and has built-in geocoding capability with Leaflet Maps API.
- [RestApi 0.0.7](https://modules.processwire.com/modules/rest-api/) Module to create a REST API with ProcessWire
- [Tracy Debugger 4.20.1](https://modules.processwire.com/modules/tracy-debugger/) Tracy debugger from Nette with several PW specific custom tools
- [Templates: Child Pages 0.1.8](https://modules.processwire.com/modules/templates-child-pages/) For any page, allows the restricting of templates that may be used for child pages.



## Mise à jour 

[Voir ici.](https://processwire.com/docs/start/install/upgrade/)

Dossiers et fichiers à changer :
- /wire/
- /index.php
- /.htaccess

Conseils :
- se connecter avec un compte super user
- activer temporairement le debug mode ($config->debug=true; dans /site/config.php)



## Ajout de contenu

- se connecter à l'[admin](http://performance-lab.huma-num.fr/carte_univ/processwire/)

La page "Universités" contient la liste de toutes les universités. Les pages "Parcours", "Accords" et "Pays" contiennent les listes de pages permettant d'ajouter des catégories aux universités.

Les champs qui permettent d'ajouter des catégories sont réciproques : si on ajoute une catégorie à une université, cette université se retrouvera listée dans la page de la catégorie. On peut : ajouter toutes les catégories pour une université sur sa propre page; ou bien aller sur la page d'un pays et ajouter toutes les universités présentes dans ce pays.

*créer une page "université"*

- ajouter une sous-page à la page "Universités"
- renseigner le nom de l'université
- cocher les catégories pour cette université (parcours, accords et pays sont confondus à cet endroit)
- sur la carte, placer le curseur à la géolocalisation correcte de l'université (il est possible d'utiliser la fonction recherche ou de copier les latitude et longitude à partir d'une autre carte)
- publier la page

*créer une page/catégorie "accord"/"parcours"/"pays"*

- ajouter une sous-page aux pages "Parcours", "Accords" ou "Pays"
- renseigner le nom de la catégorie. Pour un pays, il faut que le nom corresponde exactement à un nom présent dans [ce fichier json](https://gitlab.com/performance-laboratory/sfri-cartes-universites-partenaires/-/blob/master/export/data/countries.geo.json)
- cocher les universités entrant dans cette catégorie
- publier la page



## Licence

GNU Affero General Public License v3.0

---

[![Logo Performance Laboratory](export/img/logos/logo-pl.jpg "Lien vers le site web du Performance Laboratory")](https://performance.univ-grenoble-alpes.fr/)

[![Logo UGA](export/img/logos/logo-uga.jpg "Lien vers le site web de l'UGA")](https://www.univ-grenoble-alpes.fr/)