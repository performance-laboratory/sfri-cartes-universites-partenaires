<!DOCTYPE html>
<html lang="fr">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?= $page->title; ?></title>

	<link rel="stylesheet" type="text/css" href="<?= $config->urls->templates?>scripts/node_modules/bootstrap/dist/css/bootstrap-grid.min.css" />

	<link rel="stylesheet" type="text/css" href="<?= $config->urls->templates?>styles/main.css" />

</head>

<body>

<div class="container-fluid p-0">
	<div class="row no-gutters">

		<div class="relative order-2 col-12 order-lg-1 col-lg-7 p-3 p-sm-5">
			<div class="empty">
				<a href="<?= $homepage->url() ?>">Retour à la carte ➹</a>
			</div>
		</div>

		<div class="order-1 col-12 order-lg-2 col-lg-5 p-3 p-sm-5">

			<article>
				<h1><?= $homepage->title; ?></h1>
				<h2><?= $page->title; ?></h2>
				<?= $page->body; ?>
			</article>

		</div>
	</div>
</div>

</body>
</html>
