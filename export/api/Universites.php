<?php namespace ProcessWire;

class Universites {

  public static function test () {
    return 'test successful';
  }
  
  public static function getAll() {
    $response = new \StdClass();
    $response->universites = [];
    $universites_page  = wire('pages')->get("/universites/");

    foreach($universites_page->children as $key => $univ) {
      $obj = Universites::createObject($univ);
      // construction de l'objet à renvoyer par univ
      array_push($response->universites, $obj);
    }

    return $response;
  }

  public static function get($data) {
    $data = RestApiHelper::checkAndSanitizeRequiredParameters($data, ['id|int']);

    $response = new \StdClass();

    $univ = wire('pages')->get("/universites/")->children()->get("id=".$data->id);
    if(is_null($univ)) throw new \Exception('univ not found');

    $obj = Universites::createObject($univ);
    $response = $obj;

    return $response;
  }

  private static function createObject($univ) {
    $obj = new \StdClass();

    // recup des coordonnées map si elles existent
    $cond = $univ->map->lat == "0.000000" && $univ->map->lng == "0.000000";
    $coordinates = ($cond) ? array() : array($univ->map->lat, $univ->map->lng);
    // recup des catégories
    $cats = implode(" ", $univ->categories->explode('id'));

    $countries = wire('pages')->get("/pays/")->children;
    $result = $countries->get('universites*='.$univ->id);
    if ($result) {
      $result = $result->title;
    } else {
      $result = "";
    }

    // construction de l'objet à renvoyer
    $obj->id = $univ->id;
    $obj->title = $univ->title;
    $obj->categories = $cats;
    $obj->country = $result;
    $obj->coordinates = $coordinates;

    return $obj;
  }

}