<?php namespace ProcessWire;

require_once wire('config')->paths->RestApi . "vendor/autoload.php";
require_once wire('config')->paths->RestApi . "RestApiHelper.php";

require_once __DIR__ . "/Example.php";
require_once __DIR__ . "/Universites.php";

$routes = [
  ['OPTIONS', 'test', RestApiHelper::class, 'preflight', ['auth' => false]], // this is needed for CORS Requests
  ['GET', 'test', Example::class, 'test'],
  
  'universites' => [
    ['OPTIONS', '', RestApiHelper::class, 'preflight', ['auth' => false]], // this is needed for CORS Requests
    ['GET', 'test', Universites::class, 'test'],
    ['GET', '', Universites::class, 'getAll', ["auth" => false]],
    ['GET', '{id:\d+}', Universites::class, 'get', ["auth" => false]],
    // ['GET', '{id:\d+}', GlossaryReferences::class, 'get', ["auth" => false]], // check: https://github.com/nikic/FastRoute
  ],
];