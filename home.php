<!DOCTYPE html>
<html lang="fr">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= $page->title; ?></title>

	<link rel="stylesheet" type="text/css" href="<?= $config->urls->templates?>scripts/node_modules/bootstrap/dist/css/bootstrap-grid.min.css" />

	<!-- include leaflet -->
	<link rel="stylesheet" href="<?= $config->urls->templates?>scripts/node_modules/leaflet/dist/leaflet.css" />
	<script src="<?= $config->urls->templates?>scripts/node_modules/leaflet/dist/leaflet.js" defer></script>

	<!-- include leaflet markercluster -->
	<script src="<?= $config->urls->templates?>scripts/node_modules/leaflet.markercluster/dist/leaflet.markercluster.js" defer></script>
	<link rel="stylesheet" href="<?= $config->urls->templates?>scripts/node_modules/leaflet.markercluster/dist/MarkerCluster.css" />
	<link rel="stylesheet" href="<?= $config->urls->templates?>scripts/node_modules/leaflet.markercluster/dist/MarkerCluster.Default.css" />

	<!-- include leaflet featuregroup subgroup -->
	<script src="<?= $config->urls->templates?>scripts/node_modules/leaflet.featuregroup.subgroup/dist/leaflet.featuregroup.subgroup.js" defer></script>

	<link rel="stylesheet" type="text/css" href="<?= $config->urls->templates?>styles/main.css" />
	<script src="<?= $config->urls->templates?>scripts/main.js" defer></script>

</head>


<body>

<div class="container-fluid p-0">
	<div class="row no-gutters">

		<div class="relative order-2 col-12 order-lg-1 col-lg-7">
			<div id="mapid"></div>
		</div>

		<div class="order-1 col-12 order-lg-2 col-lg-5 p-3 p-sm-5">

			<article>
				<h1><?= $page->title; ?></h1>
				<?= $page->body; ?>
			</article>

			<aside class="p-3 p-sm-4 pb-3 pb-sm-4 px-3 px-sm-4">
				<h4>Universités</h4>
				<label>
					<input id="toggle_markers" type="checkbox"/>
					<span class="cat_title">Voir toutes les universités<span>(<?= $univ_page->children->count() ?>) (hors fonctions de filtre)</span></span>
				</label>
			</aside>

			<aside class="pb-3 pb-sm-4 px-3 px-sm-4" id="filters">
				<h4>Parcours</h4>
				<?php foreach ($parcours_page->children as $parcours): ?>
				<label>
					<input name="employee" type="checkbox" value="<?= $parcours->id ?>"/>
					<span class="cat_title"><?= $parcours->title ?><span>(<?= $parcours->references()->count() ?>)</span></span>
				</label>
				<?php endforeach; ?>

				<h4 class="mt-3 mt-sm-4">Accords</h4>
				<?php foreach ($accords_page->children as $accord): ?>
				<label>
					<input name="employee" type="checkbox" value="<?= $accord->id ?>"/>
					<span class="cat_title"><?= $accord->title ?><span>(<?= $accord->references()->count() ?>)</span></span>
				</label>
				<?php endforeach; ?>

			</aside>

			<a href="<?= $mentions_page->url() ?>"><?=$mentions_page->title?></a>

		</div>
	</div>
</div>



</body>
</html>
